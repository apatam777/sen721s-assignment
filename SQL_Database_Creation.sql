create database Indunction_system;

Create table Administrator (
AdminId int,
AdminEmail varchar(50),
AdminUserName varchar(50),
AdminPassword varchar(50),
AdminRole int,
DateCreated date,
NumberSections int,
LoggedIn bit,
Last_logged_in date,
Primary Key (AdminId)
);

Create table Users (
UserId int,
UserEmail varchar(50),
UserName varchar(50),
UserPassword varchar(50),
UserRole int,
DateCreated date,
NumberEnrollments int,
LoggedIn bit,
Last_logged_in date,
Primary Key (UserId)
);

Create table Section(
SectionId int not null Primary Key,
SectionName varchar(50),
SectionDescription varchar(300),
CreatorID int Foreign Key References Administrator(AdminId),
DateCreated date,
LastModified date,
NumberofParticipants int,
ParticipantsLoggedIn int,
NumberofUnits int,
SectionPic varchar(250)
);

Create table Enrollment(
Id int not null Primary Key,
UserId int not null Foreign Key References Users(UserId),
SectionId int not null Foreign Key References Section(SectionId)
);

Create table Unit(
UnitId int not null Primary Key,
UnitName varchar(250),
VideoId int,
DateCreated date,
NumberofParticipants int,
SectionId int not null Foreign Key References Section(SectionId),
);

Create table Video(
VideoId int not null Primary Key,
VideoLink varchar(250),
VideoName varchar(250),
VideoDescription varchar(300),
SectionId int not null Foreign Key References Section(SectionId),
UnitId int not null Foreign Key References Unit(UnitId),
DateUploaded date
);

Create table MicroCourse(
CourseId int not null Primary Key,
DocumentId int,
SectionId int not null Foreign Key References Section(SectionId),
UnitId int not null Foreign Key References Unit(UnitId),
);

Create table Document(
DocumentId int not null Primary Key,
DocumentLink varchar(250),
DocumentName varchar(250),
DocumentDescription varchar(300),
SectionId int not null Foreign Key References Section(SectionId),
UnitId int not null Foreign Key References Unit(UnitId),
CourseId int not null Foreign Key References MicroCourse(CourseId),
DateUploaded date
);

Create table Game(
GameId int not null Primary Key,
SectionId int not null Foreign Key References Section(SectionId),
UnitId int not null Foreign Key References Unit(UnitId),
);

Create table Question(
QuestionId int not null Primary Key,
Questions varchar(300),
GameId int not null Foreign Key References Game(GameId),
AnswerId int not null,
);

Create table Answer(
AnswerId int not null Primary Key,
QuestionId int not null Foreign Key References Question(QuestionId),
Answers varchar(300),
);

Create table Progress(
ProgressId int not null Primary Key,
UserId int not null Foreign Key References Users(UserId),
SectionId int not null Foreign Key References Section(SectionId),
UnitId int not null Foreign Key References Unit(UnitId),
NumberofGamesCompleted int,
ProgressPercentage float
);

Create table Score(
ScoreId int not null Primary Key,
UserId int not null Foreign Key References Users(UserId),
SectionId int not null Foreign Key References Section(SectionId),
UnitId int not null Foreign Key References Unit(UnitId),
GameId int not null Foreign Key References Game(GameId),
Score int,
HighScore int
);

ALTER TABLE Unit
ADD FOREIGN KEY (VideoId) REFERENCES Video(VideoId);

ALTER TABLE Question
ADD FOREIGN KEY (AnswerId) REFERENCES Answer(AnswerId);



